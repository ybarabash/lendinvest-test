<?php

namespace App\Exception\Invest;

use App\Exception\InvestException;

/**
 * Class LoanPeriodMismatch
 */
class LoanPeriodMismatch extends InvestException
{

}
