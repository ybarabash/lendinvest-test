<?php

namespace App\Exception\Invest;

use App\Exception\InvestException;

/**
 * Class InvestorInsufficientBalance
 */
class InvestorInsufficientBalance extends InvestException
{

}
