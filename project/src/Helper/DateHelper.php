<?php

namespace App\Helper;

/**
 * Class DateHelper
 */
class DateHelper
{

    /**
     * During how many calendar months period is?
     *
     * @param \DateTime $from
     * @param \DateTime $to
     *
     * @return int
     */
    public function getCalendarMonthNumber(\DateTime $from, \DateTime $to): int
    {
        return  ((int)$to->format('Y') - (int)$from->format('Y')) * 12 + (int)$to->format('n') - (int)$from->format('n') + 1;
    }

    /**
     * @param \DateTime $from
     *
     * @return int
     */
    public function getDaysNumberToEndOfMonth(\DateTime $from): int
    {
        return $from->diff(new \DateTime($from->format('Y-m-').$from->format('t').' 00:00:00'), true)->d + 1;
    }

    /**
     * @param \DateTime $to
     *
     * @return int
     */
    public function getDaysNumberFromStartOfMonth(\DateTime $to): int
    {
        return (new \DateTime($to->format('Y-m-').'01 00:00:00'))->diff($to, true)->d + 1;
    }

    /**
     * @param \DateTime $from
     * @param \DateTime $to
     *
     * @return int
     */
    public function getDaysNumber(\DateTime $from, \DateTime $to): int
    {
        return $from->diff($to, true)->d + 1;
    }

    /**
     * @param \DateTime $date
     *
     * @return int
     */
    public function getDaysNumberInMonthFor(\DateTime $date): int
    {
        return (int)$date->format('t');
    }
}
