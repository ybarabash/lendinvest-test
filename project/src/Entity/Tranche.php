<?php

namespace App\Entity;

use App\Exception\Invest\TrancheMaxAmountExceeded;

/**
 * Class Tranche
 */
class Tranche
{
    /**
     * @var Loan
     */
    private $loan;

    /**
     * @var float
     */
    private $interestMonthlyPercentage;

    /**
     * @var float
     */
    private $maxAmount;

    /**
     * @var float
     */
    private $amount = 0.0;

    /**
     * @param Loan  $loan
     * @param float $interestMonthlyPercentage
     * @param float $maxAmount
     */
    public function __construct(Loan $loan, float $interestMonthlyPercentage, float $maxAmount)
    {
        $this->loan = $loan;
        $this->interestMonthlyPercentage = $interestMonthlyPercentage;
        $this->maxAmount = $maxAmount;
    }

    /**
     * @param float $amount
     *
     * @return void
     *
     * @throws TrancheMaxAmountExceeded
     */
    public function credit(float $amount)
    {
        if (($this->amount + $amount) > $this->maxAmount) {
            throw new TrancheMaxAmountExceeded();
        }

        $this->amount += $amount;
    }

    /**
     * @return Loan
     */
    public function getLoan(): Loan
    {
        return $this->loan;
    }

    /**
     * @return float
     */
    public function getInterestMonthlyPercentage(): float
    {
        return $this->interestMonthlyPercentage;
    }

    /**
     * @return float
     */
    public function getMaxAmount(): float
    {
        return $this->maxAmount;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }
}
