<?php

namespace App\Entity;

use App\Exception\Invest\InvestorInsufficientBalance;
use App\Exception\Invest\LoanPeriodMismatch;
use App\Exception\Invest\TrancheMaxAmountExceeded;
use App\Exception\InvestException;

/**
 * Class Investor
 */
class Investor
{
    /**
     * @var float
     */
    private $balance;

    /**
     * @var array|Investment[]
     */
    private $investments;

    /**
     * @param float $balance
     */
    public function __construct(float $balance = 0.0)
    {
        $this->balance = $balance;
        $this->investments = [];
    }

    /**
     * @param Tranche   $tranche
     * @param float     $amount
     * @param \DateTime $date
     *
     * @return Investment
     *
     * @throws InvestException
     */
    public function invest(Tranche $tranche, float $amount, \DateTime $date): Investment
    {
        $date = (clone $date)->setTime(0, 0, 0);
        $loan = $tranche->getLoan();

        if ($date < $loan->getStartDate() || $date > $loan->getEndDate()) {
            throw new LoanPeriodMismatch();
        }

        $this->debit($amount);

        try {
            $tranche->credit($amount);
        } catch (TrancheMaxAmountExceeded $exception) {
            $this->credit($amount);

            throw $exception;
        }

        $investment = new Investment($tranche, $amount, $date);
        $this->investments[] = $investment;

        return $investment;
    }

    /**
     * @param float $amount
     *
     * @return void
     */
    public function credit(float $amount)
    {
        $this->balance += $amount;
    }

    /**
     * @param float $amount
     *
     * @return void
     *
     * @throws InvestorInsufficientBalance
     */
    public function debit(float $amount)
    {
        if (($this->balance - $amount) < 0) {
            throw new InvestorInsufficientBalance();
        }

        $this->balance -= $amount;
    }

    /**
     * @return float
     */
    public function getBalance(): float
    {
        return $this->balance;
    }

    /**
     * @return Investment[]|array
     */
    public function getInvestments()
    {
        return $this->investments;
    }
}
