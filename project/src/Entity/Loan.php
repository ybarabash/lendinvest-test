<?php

namespace App\Entity;

/**
 * Class Loan
 */
class Loan
{
    /**
     * @var \DateTime
     */
    private $startDate;

    /**
     * @var \DateTime
     */
    private $endDate;

    /**
     * @var array|Tranche[]
     */
    private $trancheCollection;

    /**
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     */
    public function __construct(\DateTime $startDate, \DateTime $endDate)
    {
        $this->startDate = (clone $startDate)->setTime(0, 0, 0);
        $this->endDate = (clone $endDate)->setTime(0, 0, 0);
        $this->trancheCollection = [];
    }

    /**
     * @param float $monthlyInterestPercentage
     * @param float $maximumInvestAmount
     *
     * @return Tranche
     */
    public function createTranche(float $monthlyInterestPercentage, float $maximumInvestAmount): Tranche
    {
        $tranche = new Tranche($this, $monthlyInterestPercentage, $maximumInvestAmount);
        $this->trancheCollection[] = $tranche;

        return $tranche;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate(): \DateTime
    {
        return $this->endDate;
    }
}
