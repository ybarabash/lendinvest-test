<?php

namespace App\Entity;

/**
 * Class Investment
 */
class Investment
{
    /**
     * @var Tranche
     */
    private $tranche;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @param Tranche   $tranche
     * @param float     $amount
     * @param \DateTime $date
     */
    public function __construct(Tranche $tranche, float $amount, \DateTime $date)
    {
        $this->tranche = $tranche;
        $this->amount = $amount;
        $this->date = (clone $date)->setTime(0, 0, 0);
    }

    /**
     * @return Tranche
     */
    public function getTranche(): Tranche
    {
        return $this->tranche;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }
}
