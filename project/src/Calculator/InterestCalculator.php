<?php

namespace App\Calculator;

use App\Entity\Investment;
use App\Entity\Investor;
use App\Helper\DateHelper;

/**
 * Class InterestCalculator
 */
class InterestCalculator
{
    /**
     * @param Investor  $investor
     * @param \DateTime $from
     * @param \DateTime $to
     *
     * @return float
     */
    public function calculate(Investor $investor, \DateTime $from, \DateTime $to): float
    {
        $from = (clone $from)->setTime(0, 0, 0);
        $to = (clone $to)->setTime(0, 0, 0);
        if ($from > $to) {
            throw new \RuntimeException('"From date" can not be after "to date"');
        }

        $amount = 0.0;
        foreach ($investor->getInvestments() as $investment) {
            $amount += $this->calculateInvestment($investment, $from, $to);
        }

        // actually floats, their calculations and round is not production solution. specific money value objects should be considered
        return round($amount, 2);
    }

    /**
     * @param Investment $investment
     * @param \DateTime  $from
     * @param \DateTime  $to
     *
     * @return float
     */
    private function calculateInvestment(Investment $investment, \DateTime $from, \DateTime $to): float
    {
        $dateHelper = new DateHelper();
        $investmentFrom = $investment->getDate();
        $investmentTo = $investment->getTranche()->getLoan()->getEndDate();

        if ($from > $investmentTo) {
            return 0.0;
        }

        if ($to < $investmentFrom) {
            return 0.0;
        }

        // calculate actual period: intersection of investment period and start-from period
        $actualFrom = ($from > $investmentFrom) ? $from : $investmentFrom;
        $actualTo = ($to < $investmentTo) ? $to : $investmentTo;

        $interestMonthlyPercentage = $investment->getTranche()->getInterestMonthlyPercentage();
        $investmentAmount = $investment->getAmount();

        $calendarMonthNumber = $dateHelper->getCalendarMonthNumber($actualFrom, $actualTo);
        // we have monthly percentage interest. for not entire months interest is calculated on days basis. as month's days count differs from month to month we do following
        if ($calendarMonthNumber === 1) {
            $amount = $this->calculatePartialMonthInterest(
                $dateHelper->getDaysNumber($actualFrom, $actualTo),
                $dateHelper->getDaysNumberInMonthFor($actualFrom),
                $interestMonthlyPercentage,
                $investmentAmount
            );
        } else {
            // amount = first month (partial) + last month (partial) + entire months (if exist)
            // first month
            $amount = $this->calculatePartialMonthInterest(
                $dateHelper->getDaysNumberToEndOfMonth($actualFrom),
                $dateHelper->getDaysNumberInMonthFor($actualFrom),
                $interestMonthlyPercentage,
                $investmentAmount
            );
            // last month
            $amount += $this->calculatePartialMonthInterest(
                $dateHelper->getDaysNumberFromStartOfMonth($actualTo),
                $dateHelper->getDaysNumberInMonthFor($actualTo),
                $interestMonthlyPercentage,
                $investmentAmount
            );
            // entire months
            $entireMonths = $calendarMonthNumber - 2;
            $amount += $this->calculateEntireMonthsInterest($entireMonths, $interestMonthlyPercentage, $investmentAmount);
        }

        return $amount;
    }

    /**
     * @param int   $days
     * @param int   $daysNumberInMonth
     * @param float $interestMonthlyPercentage
     * @param float $investmentAmount
     *
     * @return float
     */
    private function calculatePartialMonthInterest(int $days, int $daysNumberInMonth, float $interestMonthlyPercentage, float $investmentAmount): float
    {
        return ($days * $interestMonthlyPercentage * $investmentAmount) / ($daysNumberInMonth * 100);
    }

    /**
     * @param int   $monthsNumber
     * @param float $interestMonthlyPercentage
     * @param float $investmentAmount
     *
     * @return float
     */
    private function calculateEntireMonthsInterest(int $monthsNumber, float $interestMonthlyPercentage, float $investmentAmount): float
    {
        return ($monthsNumber * $interestMonthlyPercentage * $investmentAmount) / 100;
    }
}
