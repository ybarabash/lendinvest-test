<?php

namespace App\Tests;

use App\Calculator\InterestCalculator;
use App\Entity\Investor;
use App\Entity\Loan;
use App\Exception\Invest\InvestorInsufficientBalance;
use App\Exception\Invest\TrancheMaxAmountExceeded;
use App\Exception\InvestException;
use PHPUnit\Framework\TestCase;

/**
 * Class InvestTest
 */
class InvestTest extends TestCase
{
    public function test()
    {
        $loan = new Loan(new \DateTime('2015-10-01'), new \DateTime('2015-11-15'));
        $trancheA = $loan->createTranche(3, 1000);
        $trancheB = $loan->createTranche(6, 1000);
        $investor1 = new Investor(1000);
        $investor2 = new Investor(1000);
        $investor3 = new Investor(1000);
        $investor4 = new Investor(1000);

        // Case 1
        try {
            $investor1->invest($trancheA, 1000, new \DateTime('2015-10-03'));
            $this->assertEquals(0, $investor1->getBalance());
        } catch (InvestException $exception) {
            $this->fail('case failed');
        }

        // Case 2
        try {
            $investor2->invest($trancheA, 1, new \DateTime('2015-10-04'));
            $this->fail('exception expected');
        } catch (TrancheMaxAmountExceeded $exception) {
            $this->assertEquals(1000, $investor2->getBalance());
        }

        // Case 3
        try {
            $investor3->invest($trancheB, 500, new \DateTime('2015-10-10'));
            $this->assertEquals(500, $investor3->getBalance());
        } catch (InvestException $exception) {
            $this->fail('case failed');
        }

        // Case 4
        try {
            $investor4->invest($trancheB, 1100, new \DateTime('2015-10-25'));
            $this->fail('exception expected');
        } catch (InvestorInsufficientBalance $exception) {
            $this->assertEquals(1000, $investor4->getBalance());
        }

        // Calculation cases
        $calculator = new InterestCalculator();
        $from = new \DateTime('2015-10-01');
        $to = new \DateTime('2015-10-31');

        $this->assertEquals(28.06, $calculator->calculate($investor1, $from, $to));
        $this->assertEquals(21.29, $calculator->calculate($investor3, $from, $to));
    }
}
