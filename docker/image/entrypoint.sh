#!/bin/sh

cd /project
composer install
php vendor/bin/phpunit -c tests/phpunit.xml
